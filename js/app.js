(function(){
  'use strict';

  angular.module('cdbase', [
    'ui.router',
    'dashboard.controller',
  ]).config(appConfig);

  appConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
  function appConfig($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('dashboard',{
        url: '/',
        templateUrl: '/views/dashboard.html',
        controller: 'dashboardCtrl'
      })
      .state('hello',{
        url: '/hello',
        templateUrl: '/views/dashboard.html',
        controller: 'dashboardCtrl'
      });
  };
})();
